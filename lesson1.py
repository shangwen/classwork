# Lesson 1
Python is a coding language. It uses ">>>" as a prompt, and is fairly comprehensive with simple math problems.
- Addition is +
- Subtraction is -
- Multiplication is *
- Division is /
- Parentheses can be used to override PEMDAS

## Experiments
### Experiment 1: Do text files accept Chinese characters?
1. Switch keyboard to *Pinyin - Simplified*
2. Type

程尚文

3. Switch keyboard back
4. Record results

Conclusion: text files accept Chinese characters in addition to the English alphabet.

**I have come to the conclusion that this is not lesson 1**
**It's just me messing around**
